const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required!"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required!"]
	},
	email : {
		type : String,
		required : [true, "Email is required!"]
	},
	password : {
		type : String,
		required : [true, "Password is required!"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String,
		required : [true, "Mobile Number is required!"]
	},
	orderedProduct : [
			{	
				product: {
		        	type : String,
					required : [true, "UserId is required"]
		    },
				productId : {
					type : String,
					required : [true, "Product ID is required!"]
				},
				description : {
					type : String,
					required : [true, "Description Name is required!"]
				},
				purchasedOn : {
					type : Date,
					default : new Date()
				},
				Amount : {
					type : Number,
					required : [true, "Amount is required"]
				},
				status : {
					type : String,
					default : "Ordered, added to cart"
				}
			}
		]
})

module.exports = mongoose.model("User", userSchema);
